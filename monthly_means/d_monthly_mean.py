
"""This package computes monthly means from LSA SAF (daily) MSG-Disk products.

    How to use: - Modificate configuration file with your data
                - python monthly_means.py -c=configuration_file

    Conf-file: month_i = initial month
               month_f = end month
               year_i = initial year
               year_f = end year
               prod = produt name
               missing_value = missing_value for output file
               dir_out_prefix = /outpath/HDF5_out_file_
               dir_hdf = /input_path/*
               *please keep the following structure:
                /input_path/year/month/day/files.h5
    Notes Conf-file: - if you want just one month month_i=month_f
                     - if you want just one year year_i=year_f

   Author: N Simoes 2017-02-03
"""

from pathlib import Path
import configparser
import argparse
import logging
import glob
import numpy as np
import glob
import h5py


_LOG_LEVEL_STRINGS = ['CRITICAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG']
logger = logging.getLogger()


def main(n_cfile):
    """Description: This is the main function. Get the config parser,
                    get all files for user period, loop over monthly
                    files in order to compute the monthly mean and
                    finally write the output monthly mean to HDF5 file.

    Parameters:
    -----------
    n_cfile :: configuration file from argparse
    """
    # get parser
    cparse = get_confparse(n_cfile)

    # get all files
    files = get_all_files(cparse)

    # loop in files
    for m in range(len(files)):
        # Define file out
        file_out = cparse.get("composite", "dir_out_prefix") +\
                       str(m+1).zfill(2) + "010000.h5"
        # call mean dset to compute monthly mean and save it to new hdf5 file
        mean_dset(files[m], file_out, cparse)
        logger.info("File: " + file_out)


def mean_dset(files, file_out, cparse):
    """Description: compute monthly mean and save it to file.

    Parameters:
    -----------
    files :: list of montlhy files
    file_out :: output filename
    cparse :: cparse
    """
    # Get paramters from cparse: Product and Missing value
    prod = cparse.get("composite", "prod")
    mval = cparse.getint("composite", "missing_value")
    # create temporary dataset and dataset mask
    d_tmp = np.ma.masked_array(np.zeros((3712, 3712)))
    d_tmp_mask = np.ma.masked_array(np.zeros((3712, 3712)))
    # Loop in sorted files
    for f in sorted(files):
        logger.info("File: " + f)

        try:
            # get fvc dataset and missing value
            data, atrs_file, atrs_dset = get_dataset(f, prod)
            # sum dataset and mask dataset
            d_tmp += data
            d_tmp.mask = False
            d_tmp_mask += ~data.mask
        except:
            logger.warning("Something wrong with file: ", f)

    # monthly mean
    d_total = d_tmp/d_tmp_mask
    d_total.fill_value = mval
    d_total = d_total.filled()
    # write in hdf5
    write_hdf5(file_out, prod, d_total, atrs_dset, atrs_file)
    # Check if output file was writted
    logging.info(
        "OutputFile: %s exists." % file_out) if Path(file_out).is_file() else\
        logging.warning("OutputFile: %s does not exists " % file_out)


def write_hdf5(file_out, name_dset, dset, atrs_dset, atrs_file):
    """Description: write monthly mean to new hdf5.

    Parameter:
    ----------
    file_out :: output filename
    name_dset :: dataset name
    dset :: dataset array
    atrs_dset :: ItemsViewHDF5 dset attributes (get from dynamic Product)
    atrs_file :: ItemsViewHDF5 file attributes (get from dynamic Product)
    """
    # Write HDF5
    hf = h5py.File(file_out, "w")
    # Create dataset with int16 and internal compression (gzip, 9)
    dset = hf.create_dataset(name_dset, data=dset, dtype=np.int16,
                             compression="gzip", compression_opts=9)
    # Write file attributes
    hf.attrs.update(atrs_file)
    # Write dataset attributes
    dset.attrs.update(atrs_dset)
    # close hdf5
    hf.close()


def get_dataset(filename, dset):
    """Description: get dataset and dataset attributes
                    from hdf5 file.

    Parameters:
    -----------
    filename :: product filename
    dset :: dataset name

    Return:
    -------
    d_m :: dset values with mask
    attrs_file :: file attributes (ItemsViewHDF5)
    attrs_dset :: dataset attributes (ItemsViewHDF5)
    """
    # open hdf5 file and get masked dataset
    f = h5py.File(filename, "r")
    d = f[dset]
    d_v = d.value
    d_m = np.ma.masked_values(d_v, d.attrs['MISSING_VALUE'])

    # Get attributes from file and from dataset
    # Those will be used on monthly hdf5 file
    attrs_file = f.attrs.items()
    attrs_dset = d.attrs.items()
    return d_m, attrs_file, attrs_dset


def get_all_files(cparse):
    """Description: Get all files.
                    This function allows the user requiere one or
                    multiple months at the same time.

    Parameters:
    -----------
    cparse :: cparse

    Return:
    -------
    all_files :: array with all files
                 Example: [[01], [2], [3] ... ]
    """
    # get parameters
    mi = cparse.getint("composite", "month_i")
    mf = cparse.getint("composite", "month_f")
    yi = cparse.getint("composite", "year_i")
    yf = cparse.getint("composite", "year_f")

    # all files and monthly loop
    all_files = []
    for m in range(mi, mf + 1, 1):
        # month files and year loop
        month_files = []
        for y in range(yi, yf + 1, 1):
            # extend files to month files
            month_files.extend(get_files(str(y), str(m).zfill(2), cparse))
        all_files.append(month_files)
    return all_files


def get_files(year, month, cparse):
    """Description: Get HDF5 files.

    Parameters:
    -----------
    year :: year str
    month :: month str
    cparse :: cparse

    Returns:
    --------
    files :: Array with all files
    """
    # get prod
    prod = cparse.get("composite", "prod")
    # dir fvc files
    dir_hdf = cparse.get("composite", "dir_hdf")
    # get files
    dir_hdf_complete = dir_hdf + year + "/" + month + "/**/*"+prod+"*"
    files = glob.glob(dir_hdf_complete)
    return files


def get_confparse(n_cfile):
    """Description: get conf parser.

    Parameters:
    -----------
    n_cfile :: configuration filename

    Return:
    -------
    cparse :: conf parser
    """
    # Read config parser
    cparse = configparser.RawConfigParser()
    cparse.read(n_cfile)
    return cparse


def is_valid_file(parser, arg):
    """Description: Test if input conf file is valid.
                    This function is used with argparser.
    """
    return arg if Path(arg).is_file() else\
        parser.error("The file %s does not exist! " % arg)


def _log_level_string_to_int(log_level_string):
    """Description: Test log leve string given in argparse.
    """
    if not (log_level_string in _LOG_LEVEL_STRINGS):
        message = 'invalid choice: {0} (choose from'\
                      ' {1})'.format(log_level_string, _LOG_LEVEL_STRINGS)
        raise argparse.ArgumentTypeError(message)
    log_level_int = getattr(logging, log_level_string, logging.INFO)
    # check the logging log_level_choices have not changed
    # from our expected values
    assert isinstance(log_level_int, int)
    return log_level_int


def prepare_in():
    """Description: Get arguments from argparser, set logging and
                    call main function.
    """
    # get conf filename from argparse
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-c", "--conf_file", required=True,
        help="config filename.", metavar='FILE',
        type=lambda x: is_valid_file(parser, x)
    )
    parser.add_argument('-v', '--log_level', default='INFO', dest='log_level',
                        type=_log_level_string_to_int, nargs='?',
                        help='Set the logging output'
                             ' level. {0}'.format(_LOG_LEVEL_STRINGS))
    args = parser.parse_args()

    # formatter log; set configurations and get logger
    formatter = '[%(asctime)s - %(filename)s:%(lineno)s - '\
                '%(funcName)s() - %(levelname)s ] %(message)s'
    logging.basicConfig(format=formatter, level=args.log_level)
    # call main function
    main(args.conf_file)


if __name__ == "__main__":
    # call prepare in to get argparser argumentsm set logging and call main
    prepare_in()
