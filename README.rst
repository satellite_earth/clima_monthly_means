README:
-------

This package computes the monthly mean for LSASAF products (daily) and save it to HDF5 file.



Installation:
-------------

### Requirements:

* Linux
* Python 3.3 and up

`$ python setup.py install`



Usage:
------

`$ python monthly_means/d_monthly_mean.py -c configuration_file -v LOG_LEVEL`

or

`$ lsasaf_monthly_mean -c configuration_file -v LOG_LEVEL`



Contributing:
-------------

Please open an issue first to discuss what do you like to change.
