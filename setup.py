#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Setup
"""

import os

try:
    from setuptools import setup
except ImportError:
    from disutils.core import setup


# Allow setup to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

# README
README = open(os.path.join(os.path.dirname(__file__), "README.rst")).read()

setup(
    name='clima_monthly_means',
    version='1.0',
    packages=['monthly_means'],
    author='Nuno Simoes',
    author_email='nunosimoes58@gmail.com',
    description='computes monthly means from LSASAF products (daily).',
    install_requires=[
        'argparse==1.1',
        'numpy==1.15.0',
        'h5py==2.8.0'
    ],
    long_description=README,
    classifiers=[
        'Topic :: monthly means daily LSASAF products',
        'Programming Language :: Python :: 3.5.6',
    ],
    entry_points={
        'console_scripts': [
            'lsasaf_monthly_mean = monthly_means.d_monthly_mean:prepare_in',
        ],
    },
)
